#include <iostream>
#include <conio.h>
#include <string>

using namespace std;




enum CardRank 
{Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace};

enum CardSuit {Hearts, Spades, Clubs, Diamonds};

struct Card
{
	CardRank Rank;
	CardSuit Suit;
};

void PrintCard(Card card)
{
	switch (card.Rank)
	{
	case Two: cout << "The two of "; break;
	case Three: cout << "The three of "; break;
	case Four: cout << "The four of "; break;
	case Five: cout << "The five of "; break;
	case Six: cout << "The six of "; break;
	case Seven: cout << "The seven of "; break;
	case Eight: cout << "The eight of "; break;
	case Nine: cout << "The nine of "; break;
	case Ten: cout << "The ten of "; break;
	case Jack: cout << "The Jack of "; break;
	case Queen: cout << "The Queen of "; break;
	case King: cout << "The King of "; break;
	}

	switch (card.Suit)
	{
	case Hearts: cout << "Hearts\n"; break;
	case Spades: cout << "Spades\n"; break;
	case Clubs: cout << "Clubs\n"; break;
	case Diamonds: cout << "Diamonds\n"; break;
	}

}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank > card2.Rank)
	{
		return card1;
	}
	return card2;
}

int main()
{
	Card card1;
	card1.Rank = Ten;
	card1.Suit = Hearts;
	PrintCard(card1);

	Card card2;
	card2.Rank = Four;
	card2.Suit = Spades;
	PrintCard(HighCard(card1, card2));

	_getch();
	return 0;
}